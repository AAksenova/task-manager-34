package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.request.TaskShowByIdRequest;
import ru.t1.aksenova.tm.dto.response.TaskShowByIdResponse;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-id";

    @NotNull
    public static final String DESCRIPTION = "Display task by id.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @Nullable final TaskShowByIdRequest request = new TaskShowByIdRequest(getToken());
        request.setId(id);
        @Nullable final TaskShowByIdResponse response = getTaskEndpointClient().showTaskById(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
    }

}
