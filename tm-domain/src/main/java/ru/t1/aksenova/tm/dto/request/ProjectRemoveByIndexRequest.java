package ru.t1.aksenova.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectRemoveByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectRemoveByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
