package ru.t1.aksenova.tm.api.service;

import ru.t1.aksenova.tm.dto.Domain;

public interface IDomainService {

    Domain getDomain();

    void setDomain(Domain domain);

    void dataBackupLoad();

    void dataBackupSave();

    void dataBase64Load();

    void dataBase64Save();

    void dataBinaryLoad();

    void dataBinarySave();

    void dataJsonLoadFasterXml();

    void dataJsonSaveFasterXml();

    void dataJsonLoadJaxb();

    void dataJsonSaveJaxb();

    void dataXmlLoadFasterXml();

    void dataXmlSaveFasterXml();

    void dataXmlLoadJaxb();

    void dataXmlSaveJaxb();

    void dataYamlLoadFasterXml();

    void dataYamlSaveFasterXml();

}
